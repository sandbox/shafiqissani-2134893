<?php
/**
 * @file
 * Returns the HTML for the basic html structure of a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728208
 */
?><!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7" <?php print $html_attributes; ?>><![endif]-->
<!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7" <?php print $html_attributes; ?>><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8" <?php print $html_attributes; ?>><![endif]-->
<!--[if IE 8]><html class="lt-ie9" <?php print $html_attributes; ?>><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html <?php print $html_attributes . $rdf_namespaces; ?>><!--<![endif]-->

<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
    <!-- Standard Meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php if ($default_mobile_metatags): ?>
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width">
  <?php endif; ?>
  <meta http-equiv="cleartype" content="on">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" class="ui" href="http://semantic-ui.com/build/packaged/css/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="http://semantic-ui.com/stylesheets/semantic.css">
    <script>
        (function () {
            var
                eventSupport = ('querySelector' in document && 'addEventListener' in window)
            jsonSupport  = (typeof JSON !== 'undefined'),
                jQuery       = (eventSupport && jsonSupport)
                    ? '/javascript/library/jquery.min.js'
                    : '/javascript/library/jquery.legacy.min.js'
            ;
            document.write('<script src="' + jQuery + '"><\/script>');
        }());
    </script>
  <?php print $styles; ?>
  <?php print $scripts; ?>
    <script src="http://semantic-ui.com/javascript/library/history.js"></script>
    <script src="http://semantic-ui.com/javascript/library/easing.js"></script>
    <script src="http://semantic-ui.com/javascript/library/ace/ace.js"></script>
    <script src="http://semantic-ui.com/javascript/library/tablesort.js"></script>
    <script src="http://semantic-ui.com/javascript/library/waypoints.js"></script>
    <script src="http://semantic-ui.com/build/packaged/javascript/semantic.min.js"></script>


    <script src="http://semantic-ui.com/javascript/semantic.js"></script>
  <?php if ($add_html5_shim and !$add_respond_js): ?>
    <!--[if lt IE 9]>
    <script src="<?php print $base_path . $path_to_semanticui; ?>/js/html5.js"></script>
    <![endif]-->
  <?php elseif ($add_html5_shim and $add_respond_js): ?>
    <!--[if lt IE 9]>
    <script src="<?php print $base_path . $path_to_semanticui; ?>/js/html5-respond.js"></script>
    <![endif]-->
  <?php elseif ($add_respond_js): ?>
    <!--[if lt IE 9]>
    <script src="<?php print $base_path . $path_to_semanticui; ?>/js/respond.js"></script>
    <![endif]-->
  <?php endif; ?>
</head>
<body id="example" class="<?php print $classes; ?> index" <?php print $attributes;?>>
  <?php if ($skip_link_text && $skip_link_anchor): ?>
    <p id="skip-link">
      <a href="#<?php print $skip_link_anchor; ?>" class="element-invisible element-focusable"><?php print $skip_link_text; ?></a>
    </p>
  <?php endif; ?>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
